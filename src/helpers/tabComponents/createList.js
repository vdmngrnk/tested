import BasicData from "@/components/create/Index"
import Categories from "@/components/create/Categories"
import Photos from "@/components/create/Photos"
import Contacts from "@/components/create/Contacts"
import Addrress from "@/components/create/Address"
import Rooms from "@/components/create/Rooms"
import Waters from "@/components/create/Waters"
import Info from "@/components/create/Info"

// export default [
export default [{
    id: 1,
    name: BasicData,
    title: 'Основные данные',
  },
  {
    id: 2,
    name: Categories,
    title: 'Категории',
  },
  {
    id: 3,
    name: Photos,
    title: 'Фотографии',
  },
  {
    id: 4,
    name: Contacts,
    title: 'Контактные данные',
  },
  {
    id: 5,
    name: Addrress,
    title: 'Адрес',
  },
  {
    id: 6,
    name: Rooms,
    title: 'Номера',
  },
  {
    id: 7,
    name: Waters,
    title: 'Близлежащие водоемы',
  },
  {
    id: 8,
    name: Info,
    title: 'Информация для гостей',
  },
]

